function truncateDecimals (number) {
    return Math[number < 0 ? 'ceil' : 'floor'](number);
};

function update_cookie(sum_t, t_win, games) {
  console.log("update_cookie")
  my_sum_t=Number(document.cookie.split("sum_t=")[1].split(";")[0])
  my_t_win=Number(document.cookie.split("t_win=")[1].split(";")[0])
  my_games=Number(document.cookie.split("games=")[1].split(";")[0])

  my_sum_t+=Number(sum_t)
  my_t_win+=Number(t_win)
  my_games+=Number(games)

  my_p_win = truncateDecimals( my_t_win / my_games * 100)

  document.cookie = "sum_t=" + my_sum_t + ";"
  document.cookie = "avg_t=" + my_sum_t + ";"
  document.cookie = "t_win=" + my_t_win + ";"
  document.cookie = "p_win=" + my_p_win + ";"
  document.cookie = "games=" + my_games + ";"
  document.cookie = "path=/;name=stats;expires=Fri, 31 Dec 9999 23:59:59 GMT"
}

function new_cookie () {
    document.cookie = "sum_t=0;"
    document.cookie = "avg_t=0;"
    document.cookie = "t_win=0;"
    document.cookie = "p_win=0;"
    document.cookie = "games=0;"
    document.cookie = "path=/;name=stats;expires=Fri, 31 Dec 9999 23:59:59 GMT"
}


export { update_cookie, new_cookie, truncateDecimals}
